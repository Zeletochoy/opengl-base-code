#pragma once
 
#include <GL/glew.h>
#include <string>
 
class GL
{
public:
  static GLuint load_shaders(const std::string& vertex_file_path,
                             const std::string& fragment_file_path);
private:
  static GLuint load_shader(const std::string& path, const GLenum shader_type);
};
