#include <iostream>
#include <fstream>
#include <vector>
#include "gl.hh"

GLuint GL::load_shader(const std::string& path, const GLenum shader_type)
{
  GLuint shader_id = glCreateShader(shader_type);

  std::ifstream file(path, std::ios_base::in);
  std::string shader_code((std::istreambuf_iterator<char>(file)),
                          std::istreambuf_iterator<char>());
  file.close();
  
  const char *code_ptr = shader_code.c_str();
  glShaderSource(shader_id, 1, &code_ptr, nullptr);
  glCompileShader(shader_id);

  int log_length;
  glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
 
  if (log_length > 1)
  {
    std::cerr << "Error in " << path << std::endl;
    std::vector<char> error_message(log_length);
    glGetShaderInfoLog(shader_id, log_length, nullptr, &error_message[0]);
    std::cerr << error_message.data() << std::endl;
  }
  
  return shader_id;
} 
 
GLuint GL::load_shaders(const std::string& vert_path,
                        const std::string& frag_path)
{
  GLuint program_id = glCreateProgram();
  int vertex_shader_id = GL::load_shader(vert_path, GL_VERTEX_SHADER);
  int fragment_shader_id = GL::load_shader(frag_path, GL_FRAGMENT_SHADER);
  glAttachShader(program_id, vertex_shader_id);
  glAttachShader(program_id, fragment_shader_id);
  glLinkProgram(program_id);
 
  int log_length;
  glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &log_length);

  if (log_length > 1)
  {
    std::vector<char> error_message(log_length);
    glGetProgramInfoLog(program_id, log_length, nullptr, &error_message[0]);
    std::cerr << error_message.data() << std::endl;
  }
 
  glDeleteShader(vertex_shader_id);
  glDeleteShader(fragment_shader_id);
 
  return program_id;
}
