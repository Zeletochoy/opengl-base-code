#include <iostream>
#include "window.hh"

Window::Window(int w, int h, std::string title)
{
    sf::ContextSettings settings;
    settings.majorVersion = 3;
    settings.minorVersion = 3;

    window = std::make_unique<sf::Window>(sf::VideoMode(w, h), title,
                                          sf::Style::Default, settings);
}

void Window::swap_buffers()
{
    window->display();
}

void Window::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

int Window::get_w()
{
    return window->getSize().x;
}

int Window::get_h()
{
    return window->getSize().y;
}
