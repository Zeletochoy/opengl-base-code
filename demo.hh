#pragma once

#include <chrono>
#include <vector>
#include <memory>
#include <SFML/Graphics/Image.hpp>
#include "window.hh"

class Demo
{
    public:
        void run();

        Demo();
        ~Demo();

    private:
        void render();
        size_t elapsed_time();

        std::unique_ptr<Window> window;
        sf::Image texture;
        GLuint texture_id;
        GLuint uv_id;
        GLuint vertex_array_id;
        GLuint quad;
        GLuint program_id;

        std::chrono::time_point<std::chrono::high_resolution_clock> begin;
};
