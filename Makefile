CXXFLAGS = -std=c++14 -Wall -Wextra -Werror -pedantic -g
LDLIBS = -lGL -lGLEW -lsfml-window -lsfml-system -lsfml-graphics
BIN = demo
SRC = main.cc demo.cc window.cc gl.cc

$(BIN): $(SRC)
