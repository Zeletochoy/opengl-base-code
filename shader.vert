#version 330 core

layout (location = 0) in vec3 inPos;
layout (location = 1) in vec2 inUV;
out vec2 uv;

uniform float elapsed_time;
uniform vec2 resolution;
uniform float near;
uniform float far;
uniform vec3 cam;
uniform vec3 lookat;
uniform vec3 pos;
uniform vec3 size;
uniform vec3 rot;

mat4 perspective(vec2 resolution, float near, float far)
{
    vec4 c1 = vec4(2.0 * near / resolution.x, 0.0, 0.0, 0.0);
    vec4 c2 = vec4(0.0, 2.0 * near / resolution.y, 0.0, 0.0);
    vec4 c3 = vec4(0.0, 0.0, far / (near - far), far * near / (near - far));
    vec4 c4 = vec4(0.0, 0.0, 1.0, 0.0);
    return mat4(c1, c2, c3, c4);
}

mat4 translate(vec3 dir)
{
    vec4 c1 = vec4(1.0, 0.0, 0.0, 0.0);
    vec4 c2 = vec4(0.0, 1.0, 0.0, 0.0);
    vec4 c3 = vec4(0.0, 0.0, 1.0, 0.0);
    vec4 c4 = vec4(dir, 1.0);
    return mat4(c1, c2, c3, c4);
}

mat4 _rotate(float a, vec3 axis)
{
    float s = sin(a);
    float c = cos(a);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c, oc * axis.x * axis.y - axis.z * s, oc * axis.z * axis.x + axis.y * s, 0.0,
                oc * axis.x * axis.y + axis.z * s, oc * axis.y * axis.y + c, oc * axis.y * axis.z - axis.x * s, 0.0,
                oc * axis.z * axis.x - axis.y * s, oc * axis.y * axis.z + axis.x * s, oc * axis.z * axis.z + c, 0.0,
                0.0, 0.0, 0.0, 1.0);
}

mat4 rotate(vec3 angles)
{
    mat4 rot_x = _rotate(angles.x, vec3(1.0, 0.0, 0.0));
    mat4 rot_y = _rotate(angles.y, vec3(0.0, 1.0, 0.0));
    mat4 rot_z = _rotate(angles.z, vec3(0.0, 0.0, 1.0));
    return rot_x * rot_y * rot_z;
}

mat4 scale(vec3 factors)
{
    vec4 c1 = vec4(factors.x, 0.0, 0.0, 0.0);
    vec4 c2 = vec4(0.0, factors.y, 0.0, 0.0);
    vec4 c3 = vec4(0.0, 0.0, factors.z, 0.0);
    vec4 c4 = vec4(0.0, 0.0, 0.0, 1.0);
    return mat4(c1, c2, c3, c4);
}

mat4 look_at(vec3 cam, vec3 at, vec3 up)
{
    vec3 z = normalize(at - cam);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);

    vec4 c1 = vec4(x, -dot(x, cam));
    vec4 c2 = vec4(y, -dot(y, cam));
    vec4 c3 = vec4(z, -dot(z, cam));
    vec4 c4 = vec4(0.0, 0.0, 0.0, 1.0);
    return mat4(c1, c2, c3, c4);;
}

void main()
{
    mat4 mvp = scale(size) *
               translate(pos) *
               rotate(rot) *
               look_at(cam, lookat, vec3(0.0, 1.0, 0.1)) *
               perspective(resolution / resolution.x, near, far);
    gl_Position = mvp * vec4(inPos, 1.0);
    uv = inUV;
}
