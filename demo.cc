#include <string>
#include <memory>
#include <cmath>
#include "gl.hh"
#include "demo.hh"

Demo::Demo()
  : vertex_array_id(0)
  , begin(std::chrono::high_resolution_clock::now())
{
    window = std::make_unique<Window>(1080, 1080, "OpenGL 3 simple demo");
    
    glewExperimental = GL_TRUE;
    glewInit();

    glGenVertexArrays(1, &vertex_array_id);
    glBindVertexArray(vertex_array_id);

    const GLfloat quad_vertex_buffer[] =
    {
        -0.5f,  0.5f, 0.0f,
         0.5f,  0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f,

        -0.5f, -0.5f, 0.0f,
         0.5f,  0.5f, 0.0f,
         0.5f, -0.5f, 0.0f
    };

    glGenBuffers(1, &quad);
    glBindBuffer(GL_ARRAY_BUFFER, quad);
    glBufferData(GL_ARRAY_BUFFER, sizeof (quad_vertex_buffer),
                 quad_vertex_buffer, GL_STATIC_DRAW);

    const GLfloat uv_buffer[] =
    {
      0.0f, 1.0f,
      1.0f, 1.0f,
      0.0f, 0.0f,
      0.0f, 0.0f,
      1.0f, 1.0f,
      1.0f, 0.0f
    };

    glGenBuffers(1, &uv_id); 
    glBindBuffer(GL_ARRAY_BUFFER, uv_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof (uv_buffer),
                 uv_buffer, GL_STATIC_DRAW);

    program_id = GL::load_shaders("shader.vert", "shader.frag");

    texture.loadFromFile("texture.png");

    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
                 texture.getSize().x, texture.getSize().y, 0,
                 GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr());
}

Demo::~Demo()
{
    glDeleteProgram(program_id);
    glDeleteBuffers(1, &quad);
    glDeleteVertexArrays(1, &vertex_array_id);
    glDeleteTextures(1, &texture_id);
}

void Demo::run()
{
    begin = std::chrono::high_resolution_clock::now();
    for (;;)
    {
        window->clear();
        render();
        window->swap_buffers();
    }
}

void Demo::render()
{
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, quad);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
    float t = elapsed_time() / 1000.0f;

    glUseProgram(program_id);
    glUniform1f(glGetUniformLocation(program_id, "elapsed_time"), elapsed_time() / 1000.0f);
    glUniform2f(glGetUniformLocation(program_id, "resolution"), window->get_w(), window->get_h());
    glUniform1f(glGetUniformLocation(program_id, "near"), 1.0f);
    glUniform1f(glGetUniformLocation(program_id, "far"), 10.0f);
    glUniform3f(glGetUniformLocation(program_id, "cam"), 0.0f, 0.0f, 1.0f);
    glUniform3f(glGetUniformLocation(program_id, "lookat"), 0.0f, 0.0f, 0.0f);
    glUniform3f(glGetUniformLocation(program_id, "pos"), 0.5 * cos(t) - 0.5, 0.5 * sin(t) - 0.5, 0.0f);
    glUniform3f(glGetUniformLocation(program_id, "size"), 0.5f, 0.5f, 0.5f);
    glUniform3f(glGetUniformLocation(program_id, "rot"), 0.8 * t, 0.5 * t, t);
    glUniform1i(glGetUniformLocation(program_id, "tex"), 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, uv_id);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
}

size_t Demo::elapsed_time()
{
    auto now = std::chrono::high_resolution_clock::now() - begin;
    return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
 }
