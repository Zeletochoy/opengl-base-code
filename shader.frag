#version 330 core

in vec2 uv;
out vec3 fragColor;

uniform vec2 resolution;
uniform float elapsed_time;
uniform sampler2D tex;

void main()
{
    float t = mod((elapsed_time + uv.x + uv.y) / 2.0, 2.0);
    vec3 blue = vec3(0.0, 0.0, 1.0);
    vec3 red = vec3(1.0, 0.0, 0.0);
    fragColor = mix(red, blue, t < 1.0 ? t : 2.0 - t) * texture(tex, uv).rgb;
}
