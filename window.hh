#pragma once

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <string>
#include <memory>

class Window
{
    public:
        Window(int w, int h, std::string title);
        void swap_buffers();
        void clear();
        int get_w();
        int get_h();

    private:
        std::unique_ptr<sf::Window> window;
};
